# rsaLogin

#### 介绍
前端使用js加密，后台为java实现解密，使用RSA加密方式的Demo，
之前登录信息传输一直采用明文，很不安全，现在采用RSA非对称加密算法对密码进行加密，极大提升安全性。

#### 软件架构
使用SpirngBoot2.0.4 ，页面使用thymeleaf模拟登陆加密
页面js加密使用了GitHub上的js代码，地址为：https://github.com/openstack/xstatic-jsencrypt/blob/master/xstatic/pkg/jsencrypt/data/jsencrypt.js


#### 安装教程

1. git clone https://gitee.com/new_yao/rsaLogin.git
2. 导入开发软件中即可启动
3. xxxx

#### 使用说明

1. 启动类为rsalogin.App类
2. 启动后访问页面http://127.0.0.1:8787/rsaLogin/
3. xxxx

