package rsalogin.controller;

import java.security.PrivateKey;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import rsalogin.common.key.KeyManager;
import rsalogin.util.RSAUtil;
import rsalogin.util.ResultMap;

@RestController
public class LoginController {
    
    // 用户名
    private String un = "admin";
    // 密码
    private String pwd = "123456";

    @PostMapping("/login")
    public Map<String, Object> Login(String username, String rsa_password) throws Exception {
        // 将Base64编码后的私钥转换成PrivateKey对象
        PrivateKey privateKey = RSAUtil.string2PrivateKey(KeyManager.getPrivate_key());
        // 加密后的内容Base64解码
        byte[] base642Byte = RSAUtil.base642Byte(rsa_password);
        // 用私钥解密
        byte[] privateDecrypt = RSAUtil.privateDecrypt(base642Byte, privateKey);
        // 解密后的明文
        String password = new String(privateDecrypt);
        if (un.equals(username) && pwd.equals(password)) {
            return new ResultMap().success().message("登录成功");
        }
        return new ResultMap().fail().message("登录失败");
    }
    
    /**
     * 获取公钥接口
     * @return
     */
    @PostMapping("/public_key")
    public Map<String, Object> public_key(){
        return new ResultMap().success().data("public_key", KeyManager.getPublic_key());
    }
}
